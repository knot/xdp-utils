
dns-reflect.o: dns-reflect.ll
	llc -march=bpf -filetype=obj -o dns-reflect.o dns-reflect.ll

dns-reflect.ll: dns-reflect.c bpf_endian.h parsing_helpers.h
	clang -S -target bpf -Wall -Wno-unused-value -Wno-pointer-sign -Wno-compare-distinct-pointer-types -O2 -emit-llvm -c -g   -Wno-unused-command-line-argument -Icontrib/bpf $(pkg-config --cflags-only-I libknot) -DNDEBUG -o dns-reflect.ll dns-reflect.c

clean:
	rm -f dns-reflect.{o,ll}

# Useful commands:
#   eth=eth6
# Shows line with "prog/xdp id NNN" if a program is loaded.
#   ip link show $eth
#
#   sudo ip link set dev $eth xdp off
#   sudo ip link set dev $eth xdp obj ./dns-reflect.o

